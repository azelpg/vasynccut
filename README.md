# vasynccut

http://azsky2.html.xdomain.jp/

無圧縮の音声ファイル (WAV/RF64/Wave64) を、映像のフレーム単位で切り取って出力します。

映像のフレームレート (fps) がわかっていれば、映像のフレーム位置に対応する音声のサンプル位置は計算で求めることができるので、VapourSynth などで音声を処理しなくても、音声ファイル単独で、フレーム位置による切り取りを行うことが出来ます。

- 先頭の指定サンプル数をスキップすることができるので、AAC などをデコードした後に含まれている、エンコーダディレイのサンプルを除外できます。
- 音量を増減して出力することができます。

## 動作環境

Linux、ほか Unix 系 OS

C 標準ライブラリしか使っていないので、おそらく Windows 用にコンパイルすることもできます。

## ビルド・インストール

ninja コマンドが必要です。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~

## 使用例

引数で、フレーム位置の先頭と終端 (実際に含まれる終端の +1) を指定します。\
VapourSynth で clip = clip\[10:100] + clip\[110:220] などと切り取っている場合、その位置をそのまま指定します。

~~~
## 23.97 fps で 10-99 + 110-219 のフレーム位置の音声を出力
$ vasynccut -i src.wav -o out.wav -f 24000/1001 10:100 110:220

## stdout に出力して、AAC 128kbps にエンコード
$ vasynccut -i src.wav -f 30000/1001 100:200 | fdkaac -b 128 -o out.m4a -

## エンコーダディレイのサンプルを除外
$ vasynccut -i src.wav -f 24000/1001 -s 2048 -o out.wav 0:100
~~~

