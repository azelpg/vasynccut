/*$
vasynccut
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

typedef void (*FuncSetVolume)(uint8_t *buf,int n,double vol);

typedef struct
{
	int start,end;	//フレーム位置。start == -1 で終了
	int64_t offset,	//ファイルオフセット
		outsize;	//出力するバイト数
}FramePos;

typedef struct
{
	int fpsnum,				//映像fpsの分子と分母
		fpsden,
		start_sample,		//開始サンプル位置
		format,				//入力フォーマット
		samplerate,			//1秒間のサンプル数
		bytes_per_sample,	//1サンプルのバイト数
		audio_type,
		channels,			//チャンネル数
		bits,				//ビット数
		fmt_size,			//[IN] fmt データサイズ
		fmt_datsize,		//[IN] fmt 余白含むサイズ
		data_padding_size,	//[OUT] data の終端の余白サイズ
		readbuf_size,		//readbuf サイズ (サンプル単位)
		has_volume,			//音量変更を行うか
		progress_cur;		//進捗の現在位置
	int64_t data_offset,	//[IN] 音声開始位置のファイルオフセット
		output_data_size,	//[OUT] 出力のデータサイズ
		output_cur_size,	//出力中の現在サイズ
		progress_pos[10];	//進捗の各位置のサイズ
	double volume;			//音量倍率

	FILE *fpin,
		*fpout;
	const char *fname_input,
		*fname_output;	//出力ファイル。NULL で省略
	uint8_t *fmtbuf,	//WAV フォーマットデータ (余白含む)
		*readbuf;		//読み込み用バッファ
	FramePos *fposbuf;	//フレーム位置

	FuncSetVolume func_setvolume;	//音量適用関数
}AppData;

extern AppData *g_app;

enum
{
	FORMAT_UNKNOWN = -1,
	FORMAT_WAVE,
	FORMAT_WAVE64,
	FORMAT_RF64
};

enum
{
	AUDIOTYPE_PCM = 1,
	AUDIOTYPE_FLOAT = 3
};

enum
{
	APPERR_OK,
	APPERR_ALLOC,
	APPERR_OPEN_FILE,
	APPERR_UNSUPPORTED,
	APPERR_CORRUPTED
};

//

void app_exit(int ret);
void app_enderr(const char *format,...);
