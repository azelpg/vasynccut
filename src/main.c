/*$
vasynccut
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**************************************
 * main
 **************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "def.h"

//----------------

void app_get_option(int argc,char **argv);
int open_input(AppData *p,const char *filename);
void calc_frame_offset(AppData *p);
int open_output(const char *filename);
void output_cut(AppData *p);

AppData *g_app = NULL;

static const char *g_errmes[] = {
	NULL,
	"alloc error",
	"can not open file",
	"unsupported format",
	"corrupted file"
};

//----------------


/* メイン処理 */

static void _main_proc(AppData *p)
{
	int ret;

	//入力ファイルを開く

	ret = open_input(p, p->fname_input);
	if(ret)
		app_enderr("%s: '%s'", g_errmes[ret], p->fname_input);

	//ファイル位置を計算

	calc_frame_offset(p);

	//出力ファイルを開く

	ret = open_output(p->fname_output);
	if(ret)
		app_enderr("%s: '%s'", g_errmes[ret], p->fname_output);

	//切り取って出力

	output_cut(p);
}

/* AppData 初期化 */

static int _app_init(void)
{
	AppData *p;

	g_app = p = (AppData *)calloc(1, sizeof(AppData));
	if(!p) return 1;

	p->fpsnum = 24000;
	p->fpsden = 1001;

	return 0;
}

/* AppData 解放 */

static void _app_free(void)
{
	AppData *p = g_app;

	if(p->fpin) fclose(p->fpin);

	if(p->fpout && p->fpout != stdout)
		fclose(p->fpout);

	if(p->fmtbuf) free(p->fmtbuf);
	if(p->fposbuf) free(p->fposbuf);
	if(p->readbuf) free(p->readbuf);

	free(p);
}

/** main */

int main(int argc,char **argv)
{
	if(_app_init())
		app_enderr("failed init");

	app_get_option(argc, argv);

	_main_proc(g_app);

	_app_free();

	return 0;
}

/** 終了 */

void app_exit(int ret)
{
	_app_free();
	exit(ret);
}

/** エラーメッセージを表示して終了
 *
 * 最後に改行が追加される */

void app_enderr(const char *format,...)
{
	va_list ap;

	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);

	fputc('\n', stderr);

	app_exit(1);
}
