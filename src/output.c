/*$
vasynccut
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**************************************
 * 出力処理
 **************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "def.h"

//-------------------

//input.c
extern const uint8_t g_guid_RIFF[16],g_guid_WAVE[16],g_guid_FMT[16],g_guid_DATA[16];

//-------------------


//============================
// 音量適用関数
//============================


/* s16 */

static void _setvolume_s16(uint8_t *buf,int n,double vol)
{
	int v;

	for(; n; n--, buf += 2)
	{
		v = (int16_t)((buf[1] << 8) | buf[0]);
		v = (int)(v * vol + 0.5);

		if(v < INT16_MIN)
			v = INT16_MIN;
		else if(v > INT16_MAX)
			v = INT16_MAX;

		buf[0] = (uint8_t)v;
		buf[1] = (uint8_t)(v >> 8);
	}
}

/* s24 */

static void _setvolume_s24(uint8_t *buf,int n,double vol)
{
	int v;

	for(; n; n--, buf += 3)
	{
		v = (buf[2] << 16) | (buf[1] << 8) | buf[0];
		if(v & (1<<23)) v -= 1<<24;

		v = (int)(v * vol + 0.5);

		if(v < -8388608)
			v = -8388608;
		else if(v > 8388607)
			v = 8388607;

		if(v < 0) v += 1<<24;

		buf[0] = (uint8_t)v;
		buf[1] = (uint8_t)(v >> 8);
		buf[2] = (uint8_t)(v >> 16);
	}
}

/* s32 */

static void _setvolume_s32(uint8_t *buf,int n,double vol)
{
	int64_t v;

	for(; n; n--, buf += 4)
	{
		v = (int32_t)(((uint32_t)buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0]);
		v = (int64_t)(v * vol + 0.5);

		if(v < INT32_MIN)
			v = INT32_MIN;
		else if(v > INT32_MAX)
			v = INT32_MAX;

		buf[0] = (uint8_t)v;
		buf[1] = (uint8_t)(v >> 8);
		buf[2] = (uint8_t)(v >> 16);
		buf[3] = (uint8_t)(v >> 24);
	}
}


//============================
// 書き込み
//============================


/* 32bit LE 書き込み */

static void _write32le(FILE *fp,uint32_t val)
{
	uint8_t b[4];

	b[0] = (uint8_t)val;
	b[1] = (uint8_t)(val >> 8);
	b[2] = (uint8_t)(val >> 16);
	b[3] = (uint8_t)(val >> 24);

	fwrite(b, 1, 4, fp);
}

/* 64bit LE 書き込み */

static void _write64le(FILE *fp,uint64_t val)
{
	uint8_t b[8];
	int i,sf;

	for(i = 0, sf = 0; i < 8; i++, sf += 8)
		b[i] = (uint8_t)(val >> sf);

	fwrite(b, 1, 8, fp);
}

/* (WAV) 先頭ヘッダの書き込み */

static void _wav_write_header(AppData *p,FILE *fp)
{
	fwrite("RIFF", 1, 4, fp);

	_write32le(fp, 4 + 8 + p->fmt_datsize + 8 + p->output_data_size);

	fwrite("WAVEfmt ", 1, 8, fp);

	_write32le(fp, p->fmt_size);
	fwrite(p->fmtbuf, 1, p->fmt_datsize, fp); //余白含む

	fwrite("data", 1, 4, fp);
	_write32le(fp, p->output_data_size);
}

/* (RF64) 先頭ヘッダの書き込み */

static void _rf64_write_header(AppData *p,FILE *fp)
{
	fwrite("RF64", 1, 4, fp);
	_write32le(fp, (uint32_t)-1);
	fwrite("WAVE", 1, 4, fp);

	//ds64

	fwrite("ds64", 1, 4, fp);
	_write32le(fp, 28);

	_write64le(fp, 4 + 36 + 8 + p->fmt_datsize + 8 + p->output_data_size); //RF64 size
	_write64le(fp, p->output_data_size); //data size
	_write64le(fp, 0); //dummy
	_write32le(fp, 0); //配列の数

	//fmt

	fwrite("fmt ", 1, 4, fp);
	_write32le(fp, p->fmt_size);
	fwrite(p->fmtbuf, 1, p->fmt_datsize, fp); //余白含む

	//data

	fwrite("data", 1, 4, fp);
	_write32le(fp, (uint32_t)-1);
}

/* (Wave64) 先頭ヘッダの書き込み */

static void _wav64_write_header(AppData *p,FILE *fp)
{
	int n;

	//DATA を 8byte 境界にする時の余白バイト

	n = p->output_data_size & 7;

	if(n) p->data_padding_size = 8 - n;

	//RIFF

	fwrite(g_guid_RIFF, 1, 16, fp);

	_write64le(fp, 40 + 24 + p->fmt_datsize + 24 + p->output_data_size + p->data_padding_size);

	fwrite(g_guid_WAVE, 1, 16, fp);

	//fmt

	fwrite(g_guid_FMT, 1, 16, fp);
	_write64le(fp, p->fmt_size + 24);
	fwrite(p->fmtbuf, 1, p->fmt_datsize, fp); //余白含む

	//data

	fwrite(g_guid_DATA, 1, 16, fp);
	_write64le(fp, p->output_data_size + 24);
}

/* 音量適用の関数をセット */

static void _set_func_volume(AppData *p)
{
	FuncSetVolume func = NULL;
	int is_int;

	if(!p->has_volume) return;

	is_int = (p->audio_type == AUDIOTYPE_PCM);

	switch(p->bits)
	{
		case 16:
			if(is_int) func = _setvolume_s16;
			break;
		case 24:
			if(is_int) func = _setvolume_s24;
			break;
		case 32:
			if(is_int) func = _setvolume_s32;
			break;
	}

	if(!func) app_enderr("volume cannot be changed in this format");

	p->func_setvolume = func;
}

/** 出力ファイルを開き、ヘッダなどを書き込む */

int open_output(const char *filename)
{
	FILE *fp;

	_set_func_volume(g_app);

	//出力を開く

	if(!filename || strcmp(filename, "-") == 0)
		fp = stdout;
	else
	{
		fp = fopen(filename, "wb");
		if(!fp) return APPERR_OPEN_FILE;
	}

	g_app->fpout = fp;

	//ヘッダの書き込み

	if(g_app->format == FORMAT_WAVE)
		_wav_write_header(g_app, fp);
	else if(g_app->format == FORMAT_RF64)
		_rf64_write_header(g_app, fp);
	else
		_wav64_write_header(g_app, fp);

	return 0;
}


//============================
// オーディオ出力
//============================


/* 進捗の情報をセット */

static void _init_progress(AppData *p)
{
	int i;

	for(i = 0; i < 9; i++)
		p->progress_pos[i] = (i + 1) * p->output_data_size / 10;

	p->progress_pos[9] = p->output_data_size;
}

/* 進捗処理 */

static void _proc_progress(AppData *p)
{
	int cur;

	if(p->fpout == stdout) return;

	cur = p->progress_cur;

	if(cur >= 10 || p->output_cur_size < p->progress_pos[cur]) return;

	while(cur < 10 && p->output_cur_size >= p->progress_pos[cur])
	{
		if(cur & 1)
			fprintf(stderr, "%d%%", (cur + 1) * 10);
		else
			fputc('.', stderr);

		cur++;
	}

	if(cur == 10) fputc('\n', stderr);

	fflush(stderr);

	p->progress_cur = cur;
}

/* 入力から読み込んで出力 */

static void _output(AppData *p,int64_t writesize)
{
	int size,ret,bufsize,sbyte;
	uint8_t *buf;
	FuncSetVolume func_volume;

	buf = p->readbuf;
	bufsize = p->readbuf_size;
	func_volume = p->func_setvolume;
	sbyte = p->bits / 8;

	while(writesize > 0)
	{
		size = (writesize > bufsize)? bufsize: writesize;

		//読み込み (読み込めなかった分は無音)

		ret = fread(buf, 1, size, p->fpin);

		if(ret < size)
			memset(buf + ret, 0, size - ret);

		//音量

		if(func_volume)
			func_volume(buf, size / sbyte, p->volume);

		//書き込み
		
		if(fwrite(buf, 1, size, p->fpout) != size)
			app_enderr("I/O write error");

		writesize -= size;

		//進捗

		p->output_cur_size += size;

		_proc_progress(p);
	}
}

/** 各フレーム位置で切り取って出力 */

void output_cut(AppData *p)
{
	FramePos *pf;

	_init_progress(p);

	for(pf = p->fposbuf; pf->start != -1; pf++)
	{
		if(fseeko(p->fpin, pf->offset, SEEK_SET))
			fseeko(p->fpin, 0, SEEK_END);

		_output(p, pf->outsize);
	}

	//data の境界余白分を出力

	if(p->data_padding_size)
	{
		memset(p->readbuf, 0, p->data_padding_size);

		fwrite(p->readbuf, 1, p->data_padding_size, p->fpout);
	}
}

