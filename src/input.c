/*$
vasynccut
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**************************************
 * 入力処理
 **************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "def.h"


//--------------------

typedef int (*FuncGotoChunk)(FILE *fp,const char *name,const uint8_t *guid,int64_t *psize,int64_t *pdatsize);

const uint8_t g_guid_RIFF[16] = {0x72,0x69,0x66,0x66,0x2E,0x91,0xCF,0x11,0xA5,0xD6,0x28,0xDB,0x04,0xC1,0x00,0x00},
	g_guid_WAVE[16] = {0x77,0x61,0x76,0x65,0xF3,0xAC,0xD3,0x11,0x8C,0xD1,0x00,0xC0,0x4F,0x8E,0xDB,0x8A},
	g_guid_FMT[16] = {0x66,0x6D,0x74,0x20,0xF3,0xAC,0xD3,0x11,0x8C,0xD1,0x00,0xC0,0x4F,0x8E,0xDB,0x8A},
	g_guid_DATA[16] = {0x64,0x61,0x74,0x61,0xF3,0xAC,0xD3,0x11,0x8C,0xD1,0x00,0xC0,0x4F,0x8E,0xDB,0x8A},
	g_guid_pcm[16] = {0x01,0x00,0x00,0x00,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0xAA,0x00,0x38,0x9B,0x71},
	g_guid_float[16] = {0x03,0x00,0x00,0x00,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0xAA,0x00,0x38,0x9B,0x71};

//--------------------


/* LE 4byte 読み込み */

static int _read32le(FILE *fp,void *dst)
{
	uint8_t b[4];

	if(fread(b, 1, 4, fp) != 4)
		return 1;
	else
	{
		*((uint32_t *)dst) = ((uint32_t)b[3] << 24) | (b[2] << 16) | (b[1] << 8) | b[0];
		return 0;
	}
}

/* LE 8byte 読み込み */

static int _read64le(FILE *fp,void *dst)
{
	uint8_t b[8];
	uint32_t hi,lo;

	if(fread(b, 1, 8, fp) != 8)
		return 1;
	else
	{
		hi = ((uint32_t)b[7] << 24) | (b[6] << 16) | (b[5] << 8) | b[4];
		lo = ((uint32_t)b[3] << 24) | (b[2] << 16) | (b[1] << 8) | b[0];

		*((uint64_t *)dst) = ((uint64_t)hi << 32) | lo;
		
		return 0;
	}
}

/* バッファから LE 2byte 読み込み */

static int _buf16le(uint8_t *buf)
{
	return (buf[1] << 8) | buf[0];
}

/* バッファから LE 4byte 読み込み */

static uint32_t _buf32le(uint8_t *buf)
{
	return ((uint32_t)buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];
}

//-------------------

/* (WAV/RF64) 指定チャンクまで行く
 *
 * pdatsize: 余白を含むデータサイズ */

static int _wav_goto_chunk(FILE *fp,const char *name,const uint8_t *guid,int64_t *psize,int64_t *pdatsize)
{
	uint8_t c[4];
	uint32_t size,fullsize;

	while(1)
	{
		if(fread(c, 1, 4, fp) != 4
			|| _read32le(fp, &size))
			break;

		fullsize = (size + 1) & ~1; //2byte 境界

		if(memcmp(c, name, 4) == 0)
		{
			if(psize) *psize = size;
			if(pdatsize) *pdatsize = fullsize;
			
			return 0;
		}
		else
		{
			//スキップ

			//RF64 で data 以外が 64bit サイズの場合
			if(size == (uint32_t)-1) return APPERR_UNSUPPORTED;
			
			if(fseek(fp, fullsize, SEEK_CUR))
				return APPERR_CORRUPTED;
		}
	}

	return APPERR_UNSUPPORTED;
}

/* (Wave64) 指定チャンクまで行く */

static int _wav64_goto_chunk(FILE *fp,const char *name,const uint8_t *guid,int64_t *psize,int64_t *pdatsize)
{
	uint8_t c[16];
	int64_t size,fullsize;

	while(1)
	{
		if(fread(c, 1, 16, fp) != 16
			|| _read64le(fp, &size))
			break;

		size -= 24; //guid と size のサイズも含む
		fullsize = (size + 7) & ~7; //8byte 境界

		if(memcmp(c, guid, 16) == 0)
		{
			if(psize) *psize = size;
			if(pdatsize) *pdatsize = fullsize;
			
			return 0;
		}
		else
		{
			if(fseek(fp, fullsize, SEEK_CUR))
				return APPERR_CORRUPTED;
		}
	}

	return APPERR_UNSUPPORTED;
}

/* fmt チャンク読み込み */

static int _read_fmt(FILE *fp,int64_t size,int64_t datsize)
{
	uint8_t *buf;
	int n;

	if(size < 16) return APPERR_UNSUPPORTED;

	//読み込み (余白含む)

	g_app->fmtbuf = buf = (uint8_t *)malloc(datsize);
	if(!buf) return APPERR_ALLOC;

	if(fread(buf, 1, datsize, fp) != datsize)
		return APPERR_CORRUPTED;

	g_app->fmt_size = size;
	g_app->fmt_datsize = datsize;

	//フォーマット (1 = PCM, 3 = float, 0xfffe = 拡張)

	n = _buf16le(buf);

	if(n != 1 && n != 3 && n != 0xfffe)
		return APPERR_UNSUPPORTED;

	g_app->audio_type = n;

	//チャンネル数

	g_app->channels = _buf16le(buf + 2);

	//サンプリングレート

	g_app->samplerate = _buf32le(buf + 4);

	//サンプルのバイト数

	g_app->bytes_per_sample = _buf16le(buf + 12);

	//ビット数

	n = _buf16le(buf + 14);

	if(n < 16) return APPERR_UNSUPPORTED;

	g_app->bits = n;

	//------ 拡張フォーマット

	if(g_app->audio_type == 0xfffe
		&& size >= 18 && _buf16le(buf + 16) == 22)
	{
		//有効なビット数とデータのビット数が異なる
		
		if(g_app->bits != _buf16le(buf + 18))
			return APPERR_UNSUPPORTED;

		//フォーマット

		if(memcmp(buf + 24, g_guid_pcm, 16) == 0)
			n = AUDIOTYPE_PCM;
		else if(memcmp(buf + 24, g_guid_float, 16) == 0)
			n = AUDIOTYPE_FLOAT;
		else
			return APPERR_UNSUPPORTED;

		g_app->audio_type = n;
	}

	return 0;
}

/* 情報を読み込み */

static int _readinfo(FILE *fp,FuncGotoChunk func)
{
	int ret;
	int64_t size,datsize;

	//fmt チャンク

	ret = func(fp, "fmt ", g_guid_FMT, &size, &datsize);
	if(ret) return ret;
	
	ret = _read_fmt(fp, size, datsize);
	if(ret) return ret;

	//data チャンクへ

	ret = func(fp, "data", g_guid_DATA, NULL, NULL);
	if(ret) return ret;

	//開始オフセット

	g_app->data_offset = ftello(fp) + g_app->start_sample * g_app->bytes_per_sample;

	return 0;
}

/** 入力ファイルを開く */

int open_input(AppData *p,const char *filename)
{
	FILE *fp;
	uint8_t d[16];
	int ret,format;

	p->fpin = fp = fopen(filename, "rb");
	if(!fp) return APPERR_OPEN_FILE;
	
	//フォーマット判定

	format = FORMAT_UNKNOWN;

	if(fread(d, 1, 16, fp) == 16)
	{
		if(memcmp(d, "RIFF", 4) == 0
			&& memcmp(d + 8, "WAVE", 4) == 0)
		{
			//WAVE

			format = FORMAT_WAVE;
			fseeko(fp, 12, SEEK_SET);
		}
		else if((memcmp(d, "BW64", 4) == 0 || memcmp(d, "RF64", 4) == 0)
			&& memcmp(d + 8, "WAVE", 4) == 0)
		{
			//RF64

			format = FORMAT_RF64;
			fseeko(fp, 12, SEEK_SET);
		}
		else if(memcmp(d, g_guid_RIFF, 16) == 0
			&& fseeko(fp, 8, SEEK_CUR) == 0
			&& fread(d, 1, 16, fp) == 16
			&& memcmp(d, g_guid_WAVE, 16) == 0)
		{
			//Wave64
			
			format = FORMAT_WAVE64;
			fseeko(fp, 32 + 8, SEEK_SET);
		}
	}

	if(format == FORMAT_UNKNOWN)
		return APPERR_UNSUPPORTED;
	
	p->format = format;

	//データ位置まで読み込む

	ret = _readinfo(fp, (format == FORMAT_WAVE64)? _wav64_goto_chunk: _wav_goto_chunk);
	if(ret) return ret;

	//読み込みバッファを確保

	p->readbuf_size = p->bytes_per_sample * (p->samplerate / 4);

	p->readbuf = (uint8_t *)malloc(p->readbuf_size);
	if(!p->readbuf) return APPERR_ALLOC;

	return 0;
}

/** フレーム位置からファイル位置を計算 */

void calc_frame_offset(AppData *p)
{
	FramePos *buf;
	double samples_per_frame;
	int64_t st,ed,output_size;

	samples_per_frame = (double)p->samplerate * p->fpsden / p->fpsnum;
	output_size = 0;

	for(buf = p->fposbuf; buf->start != -1; buf++)
	{
		st = (int64_t)(buf->start * samples_per_frame + 0.5);
		ed = (int64_t)(buf->end * samples_per_frame + 0.5);

		buf->offset = p->data_offset + st * p->bytes_per_sample;
		buf->outsize = (ed - st) * p->bytes_per_sample;

		output_size += buf->outsize;
	}

	p->output_data_size = output_size;
}

