/*$
vasynccut
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**************************************
 * オプション処理
 **************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "getoption.h"

//--------------------

#define _VERSION_TEXT "vasynccut ver 1.0.0\nCopyright (c) 2023 Azel\n"

//--------------------


//===========================
// sub
//===========================


/* 指定文字で区切られた数値を取得
 *
 * 負の値はエラー。区切り文字がない場合、１つの値とし、ret2 = 0 とする */

static void _get_split_num(const char *str,char sp,int *ret1,int *ret2)
{
	char *pc;
	int n1,n2;

	pc = strchr(str, sp);
	if(!pc)
	{
		n1 = strtol(str, NULL, 10);
		n2 = 0;
	}
	else
	{
		n1 = strtol(str, NULL, 10);
		n2 = strtol(pc + 1, NULL, 10);
	}

	if(n1 < 0 || n2 < 0)
		app_enderr("invalid value: %s", str);

	*ret1 = n1;
	*ret2 = n2;
}


//===========================
// options
//===========================


/* --input */

static void _opt_input(char *arg)
{
	g_app->fname_input = arg;
}

/* --output */

static void _opt_output(char *arg)
{
	g_app->fname_output = arg;
}

/* --fps */

static void _opt_fps(char *arg)
{
	int n1,n2;

	_get_split_num(arg, '/', &n1, &n2);

	if(n2 == 0) n2 = 1;

	g_app->fpsnum = n1;
	g_app->fpsden = n2;
}

/* --start-sample */

static void _opt_start_sample(char *arg)
{
	int n;

	n = strtol(arg, NULL, 10);
	if(n < 0) n = 0;

	g_app->start_sample = n;
}

/* --volume */

static void _opt_volume(char *arg)
{
	//db -> 倍数
	g_app->volume = exp(strtod(arg, NULL) * 2.30258509299404568402 * 0.05);
	g_app->has_volume = 1;
}

/* バージョン情報 */

static void _opt_version(char *arg)
{
	fputs(_VERSION_TEXT, stderr);
	app_exit(0);
}

/* ヘルプを出力 */

static void _put_help(void)
{
	fputs("vasynccut [options] START:END [START:END...]\n\n"
"Based on the video frame position, the audio is cut and output.\n"
"\n"
"[Options]\n"
"  -i,--input FILE\n"
"     input filename [required]\n"
"     supports WAV/RF64/Wave64(*.w64)\n"
"  -o,--output FILE\n"
"     output filename. omit or '-' to stdout.\n"
"     same format as input.\n"
"  -f,--fps <NUM or NUM/DEN>\n"
"     frame rate of video to be synchronized (default=24000/1001)\n"
"     integer or 'numerator/denominator'\n"
"  -s,--start-sample INT\n"
"     audio start sample position (default=0)\n"
"     use when the decoded audio contains samples of the encoder delay.\n"
"  -v,--volume FLOAT\n"
"     change volume (db). only S16,S24,S32\n"
"  -h,--help     display help\n"
"  -V,--version  display version\n"
"\n"
"[Frame position]\n"
"  out = src[start1 ~ end1 - 1] + src[start2 ~ end2 - 1]...\n"
"  Specify multiple start and end positions of video frames\n"
"  in the form of 'START:END'.\n"
"  Frame position starts at 0.\n"
"  Since the frame at the end position is not included,\n"
"  specify a value that is +1 to the actual last frame position.\n"
"  If you specify more than one, join each range in order.\n"
	, stderr);
}

/* ヘルプ表示 */

static void _opt_help(char *arg)
{
	_put_help();
	app_exit(0);
}

const static goptdat g_opts[] = {
	{"input", 'i', 1, _opt_input},
	{"output", 'o', 1, _opt_output},
	{"fps", 'f', 1, _opt_fps},
	{"start-sample", 's', 1, _opt_start_sample},
	{"volume", 'v', 1, _opt_volume},
	{"help", 'h', 0, _opt_help},
	{"version", 'V', 0, _opt_version},
	{NULL, 0, 0, 0} 
};

/** オプション取得 */

void app_get_option(int argc,char **argv)
{
	FramePos *fpos;
	int ret,n1,n2;

	ret = get_options(argc, argv, g_opts);

	if(ret == -1)
		app_exit(1);

	//引数がない場合、ヘルプ

	if(argc == 1) _opt_help(NULL);

	//input は必須

	if(!g_app->fname_input)
		app_enderr("need -i,--input");

	//フレーム位置の指定がない場合

	if(ret == argc)
		app_enderr("need START:END");

	//フレーム位置取得

	g_app->fposbuf = fpos = (FramePos *)malloc(sizeof(FramePos) * (argc - ret + 1));
	if(!fpos) app_enderr("failed alloc");

	for(; ret < argc; ret++)
	{
		//':' がない場合、n2 = 0 になるが、常に n1 >= n2 になるのでエラーとなる
		_get_split_num(argv[ret], ':', &n1, &n2);

		if(n1 >= n2)
			app_enderr("invalid value: %s", argv[ret]);

		fpos->start = n1;
		fpos->end = n2;
		fpos++;
	}

	fpos->start = fpos->end = -1;
}
